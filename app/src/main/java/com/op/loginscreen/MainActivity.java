package com.op.loginscreen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnSignIn, btnSignUp, btnForgotPass;
    EditText edtxEmail, edtxPass;

    private void init() {
        this.btnSignIn = findViewById(R.id.btnSignIn);
        this.btnSignUp = findViewById(R.id.btnSignUp);
        this.btnForgotPass = findViewById(R.id.btnForgotPass);
        this.edtxEmail = findViewById(R.id.edtxEmail);
        this.edtxPass = findViewById(R.id.edtxPass);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtxEmail.getText().toString().equals("wtf@wtf.com") && edtxPass.getText().toString().equals("hunter2")) {
                    Toast.makeText(getApplicationContext(), "LET'S GOOOOOOOOOOOOOOOO", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "wrong pass, pal.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "I DIDN'T SIGN UP FOR THIS", Toast.LENGTH_SHORT).show();
            }
        });
        btnForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "GOOD LUCK LOL", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
